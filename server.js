const express = require("express");
const app = express();
const userRouter = require("./routes/userRoutes");

// Mouting routes
app.use((req, res, next) => {
  if (req.path && req.path === "/users/11") {
    next();
  } else {
    res.status(404).json({ status: 404, message: "wrong id" });
  }
});

app.get("/", (req, res, next) => {
  res.send(200);
});
app.use("/users", userRouter);

// Start Listening to server
const PORT = 3030;
app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
