const User = require("./../models/userModel");

exports.createUser = (req, res) => {
  // TODO
  res.status(200).json({ status: 200, message: "create user" });
};

exports.getUser = (req, res) => {
  const userId = req.params.id;
  console.log(`The id param is ${userId}`);
  // TODO
  res.status(200).json({ status: 200, message: "get user by id" });
};

exports.getAllUsers = (req, res) => {
  // TODO
  res.status(200).json({ status: 200, message: "get users" });
};

exports.updateUser = (req, res) => {
  // TODO
  res.status(200).json({ status: 200, message: "update user" });
};
exports.deleteUser = (req, res) => {
  // TODO
  res.status(200).json({ status: 200, message: "delete user" });
};
